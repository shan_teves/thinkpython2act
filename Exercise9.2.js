Exercise 9.2

"Quoting style"

var text = "'I'm the lawyer,' he said, 'it's my case.'";

console.log(text.replace(/(^|\W)'|'(\W|$)/g, '$1"$2'));
//"I'm the lawyer," he said, "it's my case."