Exercise 2.2

"FizzBuzz"

for (var n = 1; n <= 100; n++) {
  var output = "";
  if (n % 3 == 0)
    output += "Fizz";
  console.log(output || n);
}
